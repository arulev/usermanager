//
//  UserRealmEntity.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/29/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import RealmSwift

class User: Object {
    dynamic var name = ""
    dynamic var email = ""
    dynamic var dateOfBirth = Date()
}
