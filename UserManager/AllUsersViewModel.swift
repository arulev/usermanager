//
//  AllUsersViewModel.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/28/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

class AllUsersViewModel {
    
    // MARK: Internal Properties
    var updateScreenSignal: Signal<(), NoError>
    
    // MARK: Private Properties
    private var users = [UserItem]()
    fileprivate let userManaging: UserManaging
    fileprivate var updateScreenObserver : Observer<(), NoError>
    
    // MARK: Lifecycle
    init(userManaging: UserManaging) {
        self.userManaging = userManaging
        (updateScreenSignal, updateScreenObserver) = Signal.pipe()
        bindingObserver()
    }
    
    // MARK: Internal API
    func updateModel() {
        users = userManaging.getAllUsers()
    }
    
    func cellsCountsForSectionAtIndex(_ index: Int) -> Int {
        return users.count
    }
    
    func cellModelAtIndexPath(_ indexPath: IndexPath) -> UserItem {
        return users[indexPath.row]
    }
}

private extension AllUsersViewModel {
    func bindingObserver() {
        userManaging.updateViewModelSignal.observeValues { [weak self] () in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.updateScreenObserver.send(value: ())
        }
    }
}
