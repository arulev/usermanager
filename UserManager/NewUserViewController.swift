//
//  NewUserViewController.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/27/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import Foundation
import UIKit

class NewUserViewController : UIViewController {
    
    // MARK: Private Properties
    @IBOutlet fileprivate weak var nameTextField : UITextField!
    @IBOutlet fileprivate weak var emailTextField : UITextField!
    @IBOutlet fileprivate weak var dateOfBirthTextField : UITextField!
    fileprivate var dateOfBirth : Date?
    fileprivate let viewModel = NewUserViewModel(userManaging: UserRealmManager.sharedInstance)
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapHandler(_:)))
        self.view.addGestureRecognizer(tapGesture)
        
        bindViewModel()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let inputView = UIView(frame: CGRect(x: 0, y:0, width: self.view.frame.width, height: 240))
        let datePickerView = UIDatePicker(frame: CGRect(x: 0, y: 40, width: 0, height: 0))
        datePickerView.datePickerMode = .date
        inputView.addSubview(datePickerView)
        let doneButton = UIButton(frame: CGRect(x: inputView.frame.size.width - 80, y: 0, width: 80, height: 50))
        doneButton.setTitle("Done", for: UIControlState.normal)
        doneButton.setTitle("Done", for: UIControlState.highlighted)
        doneButton.setTitleColor(.black, for: .normal)
        doneButton.setTitleColor(.gray, for: .highlighted)
        inputView.addSubview(doneButton)
        doneButton.addTarget(self, action: #selector(doneButton(sender:)), for: UIControlEvents.touchUpInside)
        dateOfBirthTextField.inputView = inputView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: UIControlEvents.valueChanged)
    }
    
    // MARK: Actions
    @IBAction func savePressed(_ sender: UIButton) {
        
        hideKeyboard()
        
        viewModel.save(userName: nameTextField.text, userEmail: emailTextField.text, userBirth: dateOfBirth)
    }
}


// MARK: Private API
private extension NewUserViewController {
    
    func showAlert(withMessage message:String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func handleDatePicker(sender : UIDatePicker) {
        dateOfBirth = sender.date
        dateOfBirthTextField.text = Utils.getStringFromDate(date: sender.date, withFormat: "MMMM dd YYYY")
    }
    
    @objc func doneButton(sender : UIButton!) {
        hideKeyboard()
    }
    
    func resetAllInfo() {
        nameTextField.text = ""
        emailTextField.text = ""
        dateOfBirthTextField.text = ""
        dateOfBirth = nil
    }
    
    @objc func tapHandler(_ sender: UITapGestureRecognizer) {
        hideKeyboard()
    }
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
}

// MARK: Binding
private extension NewUserViewController {
    func bindViewModel() {
        
        viewModel.clearFields.observeValues { [weak self] () in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.resetAllInfo()
        }
        
        viewModel.savingError.observeValues { [weak self] userSavingError in
            guard let strongSelf = self else {
                return
            }
            
            switch userSavingError {
            case .validation(let message):
                strongSelf.showAlert(withMessage: message)
            case .saving(let boolValue):
                if !boolValue {
                    strongSelf.showAlert(withMessage: "Saving Failed")
                }
            }
        }
    }
}

