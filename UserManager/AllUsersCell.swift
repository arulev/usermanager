//
//  AllUsersCell.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/28/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import Foundation
import UIKit

class AllUsersCell: UITableViewCell {
    
    // MARK: Private Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    // MARK: Internal Properties
    var nameString: String? {
        didSet {
            nameLabel.text = nameString
        }
    }
    
    var emailString: String? {
        didSet {
            emailLabel.text = emailString
        }
    }
    
    var dateBirth: Date? {
        didSet {
            dateLabel.text = Utils.getStringFromDate(date: dateBirth!, withFormat: "MMMM dd YYYY")
        }
    }
}
