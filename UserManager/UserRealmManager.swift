//
//  UserRealmManager.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/29/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import RealmSwift
import ReactiveSwift
import Result

protocol UserManaging {
    var updateViewModelSignal : Signal<(), NoError> { get set }
    func saveUserWith(userName name: String, userEmail email: String, userBirth dateBirth: Date) -> Bool
    func getAllUsers() -> [UserItem]
}

class UserRealmManager: UserManaging {
    
    // MARK: Internal properties
    var updateViewModelSignal: Signal<(), NoError>
    static let sharedInstance : UserRealmManager = {
        let instance = UserRealmManager()
        return instance
    }()
    
    // MARK: Private Properties
    private var updateViewModelObserver: Observer<(), NoError>
    
    // MARK: Lifecycle
    init() {
        (updateViewModelSignal, updateViewModelObserver) = Signal.pipe()
    }

    // MARK: Internal API
    func saveUserWith(userName name: String, userEmail email: String, userBirth dateBirth: Date) -> Bool {
        var boolResult = true
        let user = User()
        user.name = name
        user.email = email
        user.dateOfBirth = dateBirth
        let realm = try! Realm()
        
        do {
            try realm.write {
                realm.add(user)
                updateViewModelObserver.send(value: ())
            }
        } catch {
            boolResult = false
        }
        
        return boolResult
    }
    
    func getAllUsers() -> [UserItem] {
        let realm = try! Realm()
        let users = realm.objects(User.self)
        var userItem = [UserItem]()
        
        for user in users {
            userItem.append(UserItem(userName: user.name,
                                          userEmail: user.email,
                                          userDateBirth: user.dateOfBirth))
        }
        
        return userItem
    }
}
