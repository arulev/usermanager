//
//  UserItem.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/28/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import Foundation

class UserItem {
    
    // MARK: Internal Properties
    var name: String
    var email: String
    var dateOfBirth: Date
    
    // MARK: Lifecycle
    init(userName name: String, userEmail email: String, userDateBirth dateOfBirth: Date) {
        self.name = name
        self.email = email
        self.dateOfBirth = dateOfBirth
    }
}
