//
//  Utils.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/27/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import Foundation

class Utils {
    
    class func isValidateEmail(withEmail email: String?) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: email)
    }
    
    class func getStringFromDate(date: Date, withFormat format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return  formatter.string(from: date as Date)
    }
}
