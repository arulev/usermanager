//
//  UserCoreDataManager.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/28/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import Foundation
import CoreData
import ReactiveSwift
import Result

class UserCoreDataManager: UserManaging {
    
    // MARK: Internal properties
    var updateViewModelSignal: Signal<(), NoError>
    static let sharedInstance : UserCoreDataManager = {
        let instance = UserCoreDataManager()
        return instance
    }()
    
    // MARK: Private Properties
    private var updateViewModelObserver: Observer<(), NoError>
    
    // MARK: Lifecycle
    init() {
        (updateViewModelSignal, updateViewModelObserver) = Signal.pipe()
    }
    
    // MARK: Internal API
    func saveUserWith(userName name: String, userEmail email: String, userBirth dateBirth: Date) -> Bool {
        let dataBaseManager = DatabaseManager()
        let privateContext = type(of: dataBaseManager).managedObjectPrivateContext
        let entity = NSEntityDescription.entity(forEntityName: "DbUser", in: privateContext)
        let dbUser = NSManagedObject(entity: entity!, insertInto: privateContext) as! DbUser
        dbUser.fName = name
        dbUser.fEmail = email
        dbUser.fDateOfBirth = dateBirth as NSDate?
        
        let boolResult = dataBaseManager.saveContext(privateContext)
        
        if boolResult {
            updateViewModelObserver.send(value: ())
        }
        
        return boolResult
    }
    
    func getAllUsers() -> [UserItem] {
        let dataBaseManager = DatabaseManager()
        let mainContext = type(of: dataBaseManager).managedObjectMainContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "DbUser")
        var users = [DbUser]()
        try? users = mainContext.fetch(fetchRequest as! NSFetchRequest<NSFetchRequestResult>) as! [DbUser]
        var userItem = [UserItem]()
        
        for user in users {
            userItem.append(UserItem(userName: user.fName!,
                                          userEmail: user.fEmail!,
                                          userDateBirth: user.fDateOfBirth! as Date))
        }
        
        return userItem
    }
}
