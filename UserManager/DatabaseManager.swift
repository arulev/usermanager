//
//  DatabaseManager.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/27/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import Foundation
import CoreData

class DatabaseManager {
    
    // MARK: Private Properties
    private static func makeApplicationDocumentsDirectory() -> URL {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }
    
    private static func makeManagedObjectModel() -> NSManagedObjectModel {
        let modelURL = Bundle.main.url(forResource: "DataBase", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }
    
    private static func makeCoordinator() -> NSPersistentStoreCoordinator  {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: makeManagedObjectModel())
        let url = makeApplicationDocumentsDirectory().appendingPathComponent("DataBase.sqlite")
        let failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            fatalError("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
        }
        
        return coordinator
    }
    
    
    // MARK: Public Properties
    static var managedObjectMainContext: NSManagedObjectContext = {
        let coordinator = makeCoordinator()
        var managedObjectMainContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectMainContext.persistentStoreCoordinator = coordinator
        return managedObjectMainContext
    }()
    
    static var managedObjectPrivateContext: NSManagedObjectContext = {
        var managedObjectPrivateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectPrivateContext.parent = DatabaseManager.managedObjectMainContext
        return managedObjectPrivateContext
    }()
    
    
    // MARK: - Core Data Saving support
    func saveContext(_ context:NSManagedObjectContext) -> Bool {
        var boolResult : Bool = true
        
        func save(_ localContext:NSManagedObjectContext) {
            if localContext.hasChanges {
                do {
                    try localContext.save()
                } catch {
                    boolResult = false
                    let nserror = error as NSError
                    NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                    abort()
                }
            }
        }
        
        save(context)
        save(DatabaseManager.managedObjectPrivateContext)
        save(DatabaseManager.managedObjectMainContext)
        
        return boolResult
    }
}
