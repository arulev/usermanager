//
//  AllUsersViewController.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/27/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import Foundation
import UIKit

class AllUserViewController: UIViewController {
    
    // MARK: Private Properties
    @IBOutlet fileprivate weak var tableList : UITableView!
    fileprivate var viewModel = AllUsersViewModel(userManaging: UserRealmManager.sharedInstance)
    fileprivate let cellIdentifier = "Cell"
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        bindViewModel()
        updateView()
    }
}


// MARK: UITableViewDataSource
extension AllUserViewController: UITableViewDataSource {
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return viewModel.cellsCountsForSectionAtIndex(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellModel = viewModel.cellModelAtIndexPath(indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AllUsersCell
        cell.nameString = cellModel.name
        cell.emailString = cellModel.email
        cell.dateBirth = cellModel.dateOfBirth
        return cell
    }
}


// MARK: Private API
private extension AllUserViewController {
    func setupTableView() {
        tableList.tableFooterView = UIView()
        let cellNib = UINib(nibName: "AllUsersCell", bundle: nil)
        tableList.register(cellNib, forCellReuseIdentifier: cellIdentifier)
        tableList.estimatedRowHeight = 44
    }
    
    func bindViewModel() {
        viewModel.updateScreenSignal.observeValues { [weak self] () in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.updateView()
        }
    }
    
    func updateView() {
        viewModel.updateModel()
        tableList.reloadData()
    }
}



