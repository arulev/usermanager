//
//  NewUserViewModel.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/31/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

class NewUserViewModel {
    
    // MARK: Observable properties
    let clearFields: Signal<(), NoError>
    let savingError: Signal<UserSavingError, NoError>
    
    // MARK: Private Properties
    private var users = [UserItem]()
    fileprivate let userManaging: UserManaging
    private let clearFieldsObserver: Observer<(), NoError>
    private let savingErrorObserver: Observer<UserSavingError, NoError>
    
    // MARK: Lifecycle
    init(userManaging: UserManaging) {
        self.userManaging = userManaging
        (clearFields, clearFieldsObserver) = Signal.pipe()
        (savingError, savingErrorObserver) = Signal.pipe()
    }
    
    
    // MARK: Internal API
    func save(userName name: String?, userEmail email: String?, userBirth dateOfBirth: Date?) {
        if name?.characters.count == 0 {
            savingErrorObserver.send(value: .validation("Enter name"))
        } else if email?.characters.count == 0 {
            savingErrorObserver.send(value: .validation("Enter email"))
        } else if !Utils.isValidateEmail(withEmail: email) {
            savingErrorObserver.send(value: .validation("Enter valid Email"))
        } else if dateOfBirth == nil {
            savingErrorObserver.send(value: .validation("Enter date of Birth"))
        } else {
            if userManaging.saveUserWith(userName: name!, userEmail: email!, userBirth: dateOfBirth!) {
                clearFieldsObserver.send(value: ())
            } else {
                savingErrorObserver.send(value: .saving(false))
            }
        }
    }
}

enum UserSavingError {
    case validation(String)
    case saving(Bool)
}

