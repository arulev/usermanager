//
//  UserFakeManager.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/31/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import XCTest
@testable import UserManager

class UserFakeManager: UserManaging {
    
    // MARK: Internal Properties
    var users = [UserItem]()
    
    // MARK: Internal API
    func saveUserWith(userName name: String, userEmail email: String, userBirth dateBirth: Date) -> Bool {
        if name.characters.count > 0, email.characters.count > 0 {
            return true
        } else {
            return false
        }
    }
    
    func getAllUsers() -> [UserItem] {
        for _ in 0...2 {
            users.append(UserItem(userName: "name",
                                     userEmail: "email",
                                     userDateBirth: Date()))
        }
        
        return users
    }
    
}
