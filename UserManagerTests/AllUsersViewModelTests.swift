//
//  AllUsersViewModelTests.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/31/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import XCTest
@testable import UserManager

class AllUsersViewModelTests: XCTestCase {
    
    let viewModel = AllUsersViewModel(userManaging: UserFakeManager())
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCountOfEmptyUsers() {
        let count = viewModel.cellsCountsForSectionAtIndex(0)
        XCTAssertEqual(count, 0)
    }
    
    func testCountOfNonEmptyUsers() {
        viewModel.updateModel()
        let count = viewModel.cellsCountsForSectionAtIndex(0)
        XCTAssertEqual(count, 3)
    }
}
