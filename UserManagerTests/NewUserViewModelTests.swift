//
//  NewUserViewModelTests.swift
//  UserManager
//
//  Created by Amdrey Rulev on 3/31/17.
//  Copyright © 2017 Andrey Rulev. All rights reserved.
//

import XCTest
@testable import UserManager

class NewUserViewModelTests: XCTestCase {
    
    let viewModel = NewUserViewModel(userManaging: UserFakeManager())
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testThatEmptyNameIsInvalid() {
        let validation = viewModel.validate(name: nil, email: nil, dateOfBirth: nil)
        XCTAssertTrue(!validation.valid)
    }
    
    func testThatNonEmptyNameIsValid() {
        let validation = viewModel.validate(name: "name", email: "email", dateOfBirth: nil)
        XCTAssertTrue(!validation.valid)
    }
    
    func testThatEmailIsOnlyValidWhenConformsToRFCRules() {
        let validation = viewModel.validate(name: "name", email: "email@email.com", dateOfBirth: nil)
        XCTAssertTrue(validation.valid)
    }
    
    func testThatEmptyDateIsNotValid() {
        let validation = viewModel.validate(name: "name", email: "email@email.com", dateOfBirth: "")
        XCTAssertTrue(!validation.valid)
    }
    
    func testThatNonEmptyDateIsValid() {
        let validation = viewModel.validate(name: "name", email: "email@email.com", dateOfBirth: "01.02.2017")
        XCTAssertTrue(validation.valid)
    }
    
    func testThatEmptyDataCanotBeSavedIntoDatabase() {
        let savedResult = viewModel.saveUserWith(userName: "", userEmail: "", userBirth: Date())
        XCTAssertTrue(!savedResult)
    }
    
    func testThatNonEmptyDataWillBeSavedIntoDatabase() {
        let savedResult = viewModel.saveUserWith(userName: "name", userEmail: "email@email.com", userBirth: Date())
        XCTAssertTrue(savedResult)
    }
}
